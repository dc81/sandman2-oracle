# Docker Sandman2 Oracle

Docker image for running [sandman2](https://github.com/jeffknupp/sandman2) to
get a REST interface over a Oracle database.

## Quickstart

* run the image:
  ```sh
  docker run -d \
      -e PORT=80 \
      -e ORACLE_HOSTNAME=oracle \
      -e ORACLE_DATABASE=db \
      -e ORACLE_USERNAME=username \
      -e ORACLE_PASSWORD=password \
      -e ORACLE_PORT=port \
      -p 5000:80 \
      sandman2-oracle
  ```
* visit http://localhost:5000/admin

## Usage

### Exposed ports

The `sandman2` server listens on port 80 inside the container, which is
`EXPOSE`-d by the image.

### Configuration

The following environment variables are used to configure the server:

- `ORACLE_HOSTNAME`
- `ORACLE_DATABASE`
- `ORACLE_USERNAME`
- `ORACLE_PASSWORD`
- `ORACLE_PORT`
