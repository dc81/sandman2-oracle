# Python Bullseye
FROM python:bullseye

# Update and install required packages
RUN apt-get update && \
    apt-get install -y libaio1 python3-pip && \
    rm -rf /var/lib/apt/lists/*

# Install Oracle Instant Client for Linux
WORKDIR /opt/oracle
RUN wget https://download.oracle.com/otn_software/linux/instantclient/instantclient-basiclite-linuxx64.zip && \
    unzip instantclient-basiclite-linuxx64.zip && rm -f instantclient-basiclite-linuxx64.zip && \
    cd /opt/oracle/instantclient* && rm -f *jdbc* *occi* *mysql* *README *jar uidrvci genezi adrci && \
    echo /opt/oracle/instantclient* > /etc/ld.so.conf.d/oracle-instantclient.conf && ldconfig && \
    export LD_LIBRARY_PATH=/opt/oracle/instantclient:$LD_LIBRARY_PATH

# Install Sandman2 and cx_Oracle Library
RUN python -m pip install cx_Oracle psycopg2 sandman2

# Copy start file
COPY start.sh /start.sh

# Configure the listening port
ENV PORT 80
EXPOSE 80

# Set start command
CMD ["/start.sh"]